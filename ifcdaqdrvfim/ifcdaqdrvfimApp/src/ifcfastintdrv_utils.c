#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>

#include "tscioctl.h"
#include "tsculib.h"

#include "debug.h"
#include "ifcdaqdrv.h"
#include "ifcdaqdrv_utils.h"
#include "ifcdaqdrv_adc3117.h"
#include "ifcfastintdrv_utils.h"

static const double   valid_clocks[] = {5e6, 0};

static inline ifcdaqdrv_status ifcfastintdrv_alloc_tscbuf(struct ifcdaqdrv_dev *ifcdevice, struct tsc_ioctl_kbuf_req *kbuf_req);
static inline ifcdaqdrv_status ifcfastintdrv_free_tscbuf(struct ifcdaqdrv_dev *ifcdevice, struct tsc_ioctl_kbuf_req *kbuf_req);

static inline int32_t swap_mask(struct ifcdaqdrv_dev *ifcdevice) {
    switch (ifcdevice->sample_size) {
    case 2:
        return DMA_SPACE_WS;
    case 4:
        return DMA_SPACE_DS;
    case 8:
        return DMA_SPACE_QS;
    }
    return 0;
}

/* 8-byte long word endianess conversion */
static inline void ifcdaqdrv_swap64(uint64_t* destination)
{
    uint64_t source = *destination;
    uint8_t *byte_ppo;
    uint8_t *byte_aux;

    byte_ppo = (uint8_t*) destination;
    byte_aux = (uint8_t*) &source;

    byte_ppo[0] = byte_aux[7];
    byte_ppo[1] = byte_aux[6];
    byte_ppo[2] = byte_aux[5];
    byte_ppo[3] = byte_aux[4];  
    byte_ppo[4] = byte_aux[3];
    byte_ppo[5] = byte_aux[2];
    byte_ppo[6] = byte_aux[1];
    byte_ppo[7] = byte_aux[0];
}

// static inline void INIT_SECONDARY(struct ifcdaqdrv_dev *ifcdevice, struct ifcdaqdrv_dev *secondary) {
//     secondary->card = ifcdevice->card;
//     secondary->fmc = 2;
//     secondary->node = ifcdevice->node;
//     pthread_mutex_init(&secondary->sub_lock, NULL);
// }

/* Since the IFC Fast Interlock uses both FMCs we initialize both */
ifcdaqdrv_status ifcfastintdrv_init_adc(struct ifcdaqdrv_dev *ifcdevice) {
    // Initialize the ADCs, this will
    ifcdaqdrv_status status;

    pthread_mutex_init(&ifcdevice->sub_lock, NULL);
    status = adc3117_init_adc(ifcdevice);
    if (status) 
        return status;
 
    /* Set ADC inputs to the front panel connector */
    //status = adc3117_set_adc_channel_negative_input(ifcdevice, GND); // ADC channel negative input
    status = adc3117_set_adc_channel_negative_input(ifcdevice, FROM_CONNECTOR); // ADC channel negative input
    if (status)
        return status;

    status = adc3117_set_adc_channel_positive_input(ifcdevice, FROM_CONNECTOR); // ADC channel positive input
    //status = adc3117_set_adc_channel_positive_input(ifcdevice, VCAL); // ADC channel positive input
    if (status)
        return status;

    // status = ifc_fmc_tcsr_write(ifcdevice, 0x4, 0x30); //0x80=5V, 0x30=2V
    // if (status)
    //     return status;

    // status = ifc_fmc_tcsr_write(ifcdevice, 0x3, 0xCE000000);
    // if (status)
    //     return status;
    // usleep(1000);

    // status = ifc_fmc_tcsr_setclr(ifcdevice, ADC3117_GENERAL_CONFIG_REG, 0x0, 0x3); // VCAL = 0x1 Ref 4.128V, 0x0 var_ref
    // if (status)
    //     return status;

    status = adc3117_configuration_command(ifcdevice); // Send config
    if (status)
        return status;

    /* Legacy code from VME version */

    /* Set correct pins on P2 to IN */
    //ifc_xuser_tcsr_write(ifcdevice, 0x0f, 0);
    ifc_xuser_tcsr_write(ifcdevice, 0x06, 0);
    ifc_xuser_tcsr_write(ifcdevice, 0x09, 0);
    ifc_xuser_tcsr_write(ifcdevice, 0x0f, 1<<31);
    //ifc_xuser_tcsr_write(ifcdevice, 0x04,
    //    1<<7 | 1<<8 | 1<<10 | 1<<11 | 1<<12 | 1<<14 | 1<<15 | 1<<16);
    //ifc_xuser_tcsr_write(ifcdevice, 0x07,
    //    1<<7 | 1<<8 | 1<<10 | 1<<11 | 1<<12 | 1<<14 | 1<<15 | 1<<16);
    
    // Initialize V6-S6 Link
    ifc_xuser_tcsr_write(ifcdevice, 0x24, 0);
    usleep(10*1000);
    ifc_xuser_tcsr_write(ifcdevice, 0x24, 1);
    usleep(10*1000);
    ifc_xuser_tcsr_write(ifcdevice, 0x24, 2);
    usleep(10*1000);
    ifc_xuser_tcsr_write(ifcdevice, 0x24, 3);
    usleep(10*1000);

#if 0 // ONLY VALID FOR VME VERSION
    /* Check if V6-S6 is operating OK */
    ifc_xuser_tcsr_read(ifcdevice, 0x24, &i32regval);
    if ((i32regval & 0xF000C00F) != (0xC0008003))
    {    
        LOG((LEVEL_ERROR, "Register 0x24 = 0x%08x\n", i32regval));
    }
#endif

    /* Reset and set DIRECT mode of FMC support module */
    ifc_xuser_tcsr_write(ifcdevice, 0x61, 0x05010101);
    ifc_xuser_tcsr_write(ifcdevice, 0x61, 0x0A020202);
    ifc_xuser_tcsr_write(ifcdevice, 0x62, 0x05010101);
    ifc_xuser_tcsr_write(ifcdevice, 0x62, 0x0A020202);

    /* Clear ISERDES (should be 0x00010001) */
    ifc_xuser_tcsr_write(ifcdevice, 0x63, 0x80010025);
    //ifc_xuser_tcsr_write(ifcdevice, 0x63, 0x00010001);
    usleep(100000);
    ifc_xuser_tcsr_write(ifcdevice, 0x63, 0x00000000);

    INFOLOG(("Successfully started ADC3117\n"));

    return status_success;
}

void ifcfastintdrv_history_reset(struct ifcdaqdrv_dev *ifcdevice) {

        /* Turn off history to clear history errors */
        ifc_xuser_tcsr_setclr(ifcdevice, IFCFASTINT_FSM_MAN_REG, 0, IFCFASTINT_FSM_MAN_HISTORY_ENA_MASK);

        /* Turn on history buffer acq, keeping the actual HISTORY MODE */
        ifc_xuser_tcsr_setclr(ifcdevice, IFCFASTINT_FSM_MAN_REG, IFCFASTINT_FSM_MAN_HISTORY_ENA_MASK, 0);

}

/* Debug function that prints a human readable string of the status register */
void ifcfastint_print_status(int32_t reg) {
        int32_t tmp;
        int32_t i;

        printf("Status FSM: %08x, ", reg);

        tmp = (reg & IFCFASTINT_FSM_MAN_FSM_CMD_MASK) >> IFCFASTINT_FSM_MAN_FSM_CMD_SHIFT;
        switch(tmp) {
        case 0:
                printf("CMD: No action");
                break;
        }
        printf(", ");
        printf("State: ");
        tmp = (reg & IFCFASTINT_FSM_MAN_FSM_STA_MASK) >> IFCFASTINT_FSM_MAN_FSM_STA_SHIFT;
        switch(tmp) {
        case 0:
                printf("IDLE");
                break;
        case 1:
                printf("ABO");
                break;
        case 2:
                printf("PRE");
                break;
        case 3:
                printf("RUN");
                break;
        }
        printf(", ");
        tmp = (reg & IFCFASTINT_FSM_MAN_FSM_OUT_MASK) >> IFCFASTINT_FSM_MAN_FSM_OUT_SHIFT;
        printf("OUT: ");
        for(i=0; i<4; ++i) {
                if(tmp & 1<<i) {
                        printf("1");
                } else {
                        printf("0");
                }
        }
        printf(", ");
        printf("Dynamic Analog: ");
        if(reg & IFCFASTINT_FSM_MAN_DYN_ANALOG_OPT_ENA_MASK) {
                printf("On");
        } else {
                printf("Off");
        }
        printf(", ");
        printf("Dynamic Digital: ");
        if(reg & IFCFASTINT_FSM_MAN_DYN_DIGITAL_OPT_ENA_MASK) {
                printf("On");
        } else {
                printf("Off");
        }
        printf(", ");
        tmp = (reg & IFCFASTINT_FSM_MAN_FSM_FRQ_MASK) >> IFCFASTINT_FSM_MAN_FSM_FRQ_SHIFT;
        switch(tmp) {
        case 0:
                printf("Disabled");
                break;
        case 1:
                printf("1MHz");
                break;
        case 2:
                printf("500kHz");
                break;
        case 3:
                printf("200kHz");
                break;
        }
        printf(", ");
        printf("History EN: ");
        if(reg & IFCFASTINT_FSM_MAN_HISTORY_ENA_MASK) {
                printf("On");
        } else {
                printf("Off");
        }
        printf(", ");
        tmp = (reg & IFCFASTINT_FSM_MAN_HISTORY_MODE_MASK) >> IFCFASTINT_FSM_MAN_HISTORY_MODE_SHIFT;
        printf("History Mode: ");
        switch(tmp) {
        case 0:
                printf("RUN");
                break;
        case 1:
                printf("PRE+RUN");
                break;
        case 2:
                printf("IDLE+PRE+RUN");
                break;
        default:
                printf("Reserved");
                break;
        }
        printf(", ");
        printf("History SMEM error: ");
        if(reg & IFCFASTINT_FSM_MAN_HISTORY_OVER_FF_MASK) {
                printf("Set");
        } else {
                printf("Clear");
        }
        printf(", ");
        printf("History Ring overflow: ");
        if(reg & IFCFASTINT_FSM_MAN_HISTORY_RING_OVER_FF_MASK) {
                printf("Set");
        } else {
                printf("Clear");
        }
        printf(", ");
        tmp = (reg & IFCFASTINT_FSM_MAN_HISTORY_STATUS_MASK) >> IFCFASTINT_FSM_MAN_HISTORY_STATUS_SHIFT;
        printf("History Status: ");
        switch(tmp) {
        case 0:
                printf("IDLE");
                break;
        case 1:
                printf("Running");
                break;
        case 2:
                printf("Filling");
                break;
        case 3:
                printf("Ended");
                break;
        }

        printf("\n");
}

/* Register ADC3117 FMC */

ifcdaqdrv_status ifcfastintdrv_register(struct ifcdaqdrv_dev *ifcdevice){
    
    ifcdaqdrv_status status;
    status = adc3117_get_signature(ifcdevice, NULL, NULL, &ifcdevice->board_id);
    if (status)
        return status;

    /* Activate FMC */
    ifc_fmc_tcsr_write(ifcdevice, 0, 0x31170000);
    usleep(10000);

    ifcdevice->init_adc = ifcfastintdrv_init_adc;
    ifcdevice->nchannels = 16; //20 is not working !!!
    ifcdevice->smem_size = 4*1024*1024; //4 MB
    ifcdevice->vref_max = 10.24;
    ifcdevice->sample_resolution = 16;
    ifcdevice->sample_size = 2; // Important for endianess
    ifcdevice->poll_period = 1000;

    /* Functions to configure clock */
    ifcdevice->set_clock_frequency   = adc3117_set_clock_frequency;
    ifcdevice->get_clock_frequency   = adc3117_get_clock_frequency;
    ifcdevice->set_clock_source      = adc3117_set_clock_source;
    ifcdevice->get_clock_source      = adc3117_get_clock_source;
    ifcdevice->set_clock_divisor     = adc3117_set_clock_divisor;
    ifcdevice->get_clock_divisor     = adc3117_get_clock_divisor;

    memcpy(ifcdevice->valid_clocks, valid_clocks, sizeof(valid_clocks));
    ifcdevice->divisor_max = 1; //1045;
    ifcdevice->divisor_min = 1; //1;

    return status_success;
}

ifcdaqdrv_status ifcfastintdrv_read_pp_conf(struct ifcdaqdrv_dev *ifcdevice, uint32_t addr, uint64_t *pp_options) {
    
    ifcdaqdrv_status status;
    
    /* Prepares the command word for CPU copy|read using tsclib (based on TscMon source code)*/
    int cmdword = RDWR_MODE_SET( 0x44, RDWR_SPACE_USR1, 0);
    void *mybuffer = calloc(1024*1024,1);

    /* if running on little endian machine the command word needs to be converted */
    if (!ifcdaqdrv_is_byte_order_ppc()) {
        cmdword = tsc_swap_32(cmdword);
    }

    /* address that holds the configuration PP_OPTION for the specific pp block */
    ulong src_addr = IFCFASTINT_SRAM_PP_OFFSET + addr;

    /* using tsclib to read what is in SRAM 1 area */
    usleep(1000);
    status = tsc_read_blk(ifcdevice->node, src_addr, (char*) mybuffer, sizeof(*pp_options), cmdword);

    if (status) {
        LOG((LEVEL_ERROR,"tsc_blk_read() returned %d\n", status));
        free(mybuffer);
        return status;
    }

    /* Copying 8 bytes to the 64 bit variable that will be decoded */
    memcpy((void *)pp_options, mybuffer, sizeof(*pp_options));

    /* if running on BIG ENDIAN machine the 8-byte stream needs to be swapped */
    if (ifcdaqdrv_is_byte_order_ppc()) {
    	ifcdaqdrv_swap64(pp_options);
    }
    free(mybuffer);

    return status_success;
}

ifcdaqdrv_status ifcfastintdrv_write_pp_conf(struct ifcdaqdrv_dev *ifcdevice, uint32_t addr, uint64_t pp_options) {
    ifcdaqdrv_status status;
    void *mybuffer = calloc(1024*1024,1);

    /* if running on BIG endian machine, the 8-byte long word needs to be converted to big endian */
    if (ifcdaqdrv_is_byte_order_ppc()) {
    	ifcdaqdrv_swap64(&pp_options);
    }
    memcpy(mybuffer, (void *)&pp_options, sizeof(pp_options));

    /* Sets the PP_OPTION addres of the specific pp block */
    ulong dest_addr = IFCFASTINT_SRAM_PP_OFFSET + addr;

    /* Prepares the command word for CPU copy using tsclib (based on TscMon source code)*/
    int cmdword = RDWR_MODE_SET( 0x44, RDWR_SPACE_USR1, 0);
    
    /* if running on little endian machine the command word needs to be converted */
    if (!ifcdaqdrv_is_byte_order_ppc()) {
        cmdword = tsc_swap_32(cmdword);
    }

    usleep(1000);
    status = tsc_write_blk(ifcdevice->node, dest_addr, (char*) mybuffer, sizeof(pp_options), cmdword);
    
    free(mybuffer);

    if (status) {
        LOG((LEVEL_ERROR,"tsc_blk_write() returned %d\n", status));
        return status;
    }

    return status_success;
}

ifcdaqdrv_status ifcfastintdrv_read_pp_status(struct ifcdaqdrv_dev *ifcdevice, uint32_t addr, uint64_t *pp_status) {
    
    ifcdaqdrv_status status;
    
    /* Prepares the command word for CPU copy|read using tsclib (based on TscMon source code)*/
    int cmdword = RDWR_MODE_SET( 0x44, RDWR_SPACE_USR1, 0);
    void *mybuffer = calloc(1024*1024,1);

    /* if running on little endian machine the command word needs to be converted */
    if (!ifcdaqdrv_is_byte_order_ppc()) {
        cmdword = tsc_swap_32(cmdword);
    }

    /* address that holds the configuration PP_OPTION for the specific pp block */
    ulong src_addr = IFCFASTINT_SRAM_PP_STATUS + addr;

    /* using tsclib to read what is in SRAM 1 area */
    usleep(1000);
    status = tsc_read_blk(ifcdevice->node, src_addr, (char*) mybuffer, sizeof(*pp_status), cmdword);

    if (status) {
        LOG((LEVEL_ERROR,"tsc_blk_read() returned %d\n", status));
        free(mybuffer);
        return status;
    }

    /* Copying 8 bytes to the 64 bit variable that will be decoded */
    memcpy((void *)pp_status, mybuffer, sizeof(*pp_status));

    /* if running on BIG ENDIAN machine the 8-byte stream needs to be swapped */
    // if (ifcdaqdrv_is_byte_order_ppc()) {
    // 	ifcdaqdrv_swap64(pp_status);
    // }
    free(mybuffer);

    return status_success;
}


static inline ifcdaqdrv_status ifcfastintdrv_alloc_tscbuf(struct ifcdaqdrv_dev *ifcdevice, struct tsc_ioctl_kbuf_req *kbuf_req) {
    int ret;

    if (!kbuf_req) {
        return status_internal;
    }

    do {
        LOG((LEVEL_INFO, "Trying to allocate %d bytes in kernel\n", kbuf_req->size));
        ret = tsc_kbuf_alloc(ifcdevice->node, kbuf_req);
    } while ((ret < 0) && (kbuf_req->size >>= 1) > 0);

    if(ret) {
        fprintf(stderr, "Impossible to allocate tsc kbuf\n");
        return status_internal;
    }

    /* Mapping kbuf in userspace */
    if (tsc_kbuf_mmap(ifcdevice->node, kbuf_req) < 0)
    {
        /* Free the kbuf */
        tsc_kbuf_free(ifcdevice->node, kbuf_req);
        fprintf(stderr, "ERROR: tsc_kbuf_mmap(kbuf_req) failed\n");
        return status_internal;
    }

    return status_success;
}

static inline ifcdaqdrv_status ifcfastintdrv_free_tscbuf(struct ifcdaqdrv_dev *ifcdevice, struct tsc_ioctl_kbuf_req *kbuf_req) {
    if (tsc_kbuf_free(ifcdevice->node, kbuf_req))
    {
        fprintf(stderr, "[ERROR] tsclib returned error when trying tsc_kbuf_free\n");
        return status_internal;
    }
    return status_success;
}


ifcdaqdrv_status ifcfastintdrv_dma_allocate(struct ifcdaqdrv_dev *ifcdevice) {
    //void *p;
    int ret;

    /* Allocate tsc kbuf for the DMA operations */
    ifcdevice->smem_dma_buf = calloc(1, sizeof(struct tsc_ioctl_kbuf_req));
    if (!ifcdevice->smem_dma_buf) {
        return status_internal;
    }

    // Try to allocate as large dma memory as possible
    ifcdevice->smem_dma_buf->size = ifcdevice->smem_size;
    do {
        LOG((LEVEL_INFO, "Trying to allocate %d bytes in kernel\n", ifcdevice->smem_dma_buf->size));
        ret = tsc_kbuf_alloc(ifcdevice->node, ifcdevice->smem_dma_buf);
    } while ((ret < 0) && (ifcdevice->smem_dma_buf->size >>= 1) > 0);

    if(ret) {
        free(ifcdevice->smem_dma_buf);
        return status_internal;
    }
   

    /* Map kernel space in userspace */
    LOG((5, "Trying to mmap %dkiB in kernel for SMEM acquisition\n", ifcdevice->smem_dma_buf->size / 1024));
    if (tsc_kbuf_mmap(ifcdevice->node, ifcdevice->smem_dma_buf) < 0)
    {
        /* Free the kbuf */
        tsc_kbuf_free(ifcdevice->node, ifcdevice->smem_dma_buf);
        free(ifcdevice->smem_dma_buf);
        fprintf(stderr, "ERROR: tsc_kbuf_mmap(ifcdevice->smem_dma_buf) failed\n");
        return status_internal;
    }
    INFOLOG(("Successfully allocated space in kernel! [%d bytes]\n",ifcdevice->smem_dma_buf->size));
    return status_success;
}

uint64_t u64_setclr(uint64_t input, uint64_t bits, uint64_t mask, uint32_t offset) {
    uint64_t output = input & ~(mask << offset); // Clear mask
    output |= bits << offset; // Set bits
    return output;
}

ifcdaqdrv_status ifcfastintdrv_printregister(uint64_t *pp_register) {
    uint64_t tmp;
    uint32_t value;

    tmp = (*pp_register & IFCFASTINT_ANALOGPP_REG_PPACT_MASK) >> IFCFASTINT_ANALOGPP_REG_PPACT_SHIFT;
    value = (uint32_t) tmp;
    printf("pp_option_ACT  =   %d\n", value);

    tmp = (*pp_register & IFCFASTINT_ANALOGPP_REG_PPEMU_MASK) >> IFCFASTINT_ANALOGPP_REG_PPEMU_SHIFT;
    value = (uint32_t) tmp;
    printf("pp_option_EMUL =   %d\n", value);

    tmp = (*pp_register & IFCFASTINT_ANALOGPP_REG_PPMODE_MASK) >> IFCFASTINT_ANALOGPP_REG_PPMODE_SHIFT;
    value = (uint32_t) tmp;
    printf("pp_option_MODE = 0x%04x\n", value);

    tmp = (*pp_register & IFCFASTINT_ANALOGPP_REG_PPVAL1_MASK) >> IFCFASTINT_ANALOGPP_REG_PPVAL1_SHIFT;
    value = (uint32_t) tmp;
    printf("pp_option_VAL1 = 0x%08x\n", value);

    tmp = (*pp_register & IFCFASTINT_ANALOGPP_REG_PPVAL2_MASK) >> IFCFASTINT_ANALOGPP_REG_PPVAL2_SHIFT;
    value = (uint32_t) tmp;
    printf("pp_option_VAL2 = 0x%08x\n", value);

    tmp = (*pp_register & IFCFASTINT_ANALOGPP_REG_PPCVAL_MASK) >> IFCFASTINT_ANALOGPP_REG_PPCVAL_SHIFT;
    value = (uint32_t) tmp;
    printf("pp_option_CVAL = 0x%08x\n", value);

    printf("\n");
    return status_success;
}

#define TSCDMA_PKT_SIZE 0x400 // 
#define CURR_DMA_CHAN DMA_CHAN_2

ifcdaqdrv_status ifcfastintdrv_read_smem_historybuffer( struct ifcdaqdrv_dev *ifcdevice, 
                                                        void *dest_buffer, 
                                                        struct tsc_ioctl_kbuf_req *dma_buf, 
                                                        uint32_t smem_addr, 
                                                        uint32_t size) 
{
    //return status_success;

    ifcdaqdrv_status status;
    struct tsc_ioctl_dma_req dma_req = {0};

    dma_req.src_addr = (uint64_t) smem_addr;
    dma_req.src_space = 2; //DMA_SPACE_SMEM
    dma_req.src_mode = 0; //DMA_PCIE_RR2;

    dma_req.des_addr = dma_buf->b_base;
    dma_req.des_space = ifcdaqdrv_is_byte_order_ppc() ? DMA_SPACE_PCIE : DMA_SPACE_PCIE1,
    dma_req.des_mode = 0; //DMA_PCIE_RR2;

    dma_req.end_mode   = 0;
    dma_req.start_mode = (char) DMA_START_CHAN(CURR_DMA_CHAN);
    //dma_req.intr_mode  = DMA_INTR_ENA;
    dma_req.wait_mode  = DMA_WAIT_INTR | DMA_WAIT_1S | (5<<4);

    //dma_req.size = size; //dma_buf->size | DMA_SIZE_PKT_128;
    dma_req.size = size;

    status = tsc_dma_alloc(ifcdevice->node, CURR_DMA_CHAN);
    if (status) 
    {
        LOG((LEVEL_ERROR, "%s() tsc_dma_alloc(CURR_DMA_CHAN) == %d \n", __FUNCTION__, status));
        tsc_dma_clear(ifcdevice->node, CURR_DMA_CHAN);
        tsc_dma_free(ifcdevice->node, CURR_DMA_CHAN);
        return status;
    }

    status = tsc_dma_move(ifcdevice->node, &dma_req);
    if (status) 
    {
        LOG((LEVEL_ERROR, "%s() tsc_dma_move() == %d status = 0x%08x\n", __FUNCTION__, status, dma_req.dma_status));
        tsc_dma_clear(ifcdevice->node, CURR_DMA_CHAN);
        tsc_dma_free(ifcdevice->node, CURR_DMA_CHAN);
        return status;
    }

    /* Check for errors */
    if (dma_req.dma_status & DMA_STATUS_TMO)
    {
        LOG((LEVEL_ERROR, "DMA ERROR -> timeout | status = %08x\n",  dma_req.dma_status));
        tsc_dma_clear(ifcdevice->node, CURR_DMA_CHAN);
        tsc_dma_free(ifcdevice->node, CURR_DMA_CHAN);
        return status;
    }
    else if(dma_req.dma_status & DMA_STATUS_ERR)
    {
        LOG((LEVEL_ERROR, "DMA ERROR -> unknown error | status = %08x\n",  dma_req.dma_status));
        tsc_dma_clear(ifcdevice->node, CURR_DMA_CHAN);
        tsc_dma_free(ifcdevice->node, CURR_DMA_CHAN);
        return status;
    }
    
    /*free DMA channel 0 */
    status = tsc_dma_free(ifcdevice->node, CURR_DMA_CHAN);
    if (status) 
    {
      LOG((LEVEL_ERROR, "%s() tsc_dma_free() == %d\n", __FUNCTION__, status));
      tsc_dma_clear(ifcdevice->node, CURR_DMA_CHAN);
      return status;
    }

    /* Copy from kernel space (dma_buf) to userspace (pp_options)
     * TODO: check for errors and fill buffer with zeros??
     */       
    memcpy(dest_buffer, dma_buf->u_base, (size_t) size);
    return 0;
}



#define MEAS_DMA_CHAN DMA_CHAN_0
ifcdaqdrv_status ifcfastintdrv_read_sram_measurements( struct ifcdaqdrv_dev *ifcdevice, 
                                                        void *dest_buffer, 
                                                        uint32_t sram_addr) 
{
#if 0 // MEAS_WITH_DMA
    ifcdaqdrv_status status;
    struct tsc_ioctl_dma_req dma_req = {0};
    struct tsc_ioctl_kbuf_req *dma_buf;

    /* Allocate a new kbuf handler structure */
    dma_buf = calloc(1, sizeof(struct tsc_ioctl_kbuf_req));
    dma_buf->size = 0x400; 

    /* Try to allocate kernel buffer */
    status = ifcfastintdrv_alloc_tscbuf(ifcdevice, dma_buf);
    if (status) {
        free(dma_buf);
        LOG((LEVEL_ERROR, "%s() tsc_kbuf_alloc() failed\n", __FUNCTION__));
        return status_internal;
    }

    dma_req.src_addr = (uint64_t) sram_addr;
    dma_req.src_space = 4; //
    dma_req.src_mode = 0; //DMA_PCIE_RR2;

    dma_req.des_addr = dma_buf->b_base;
    dma_req.des_space = ifcdaqdrv_is_byte_order_ppc() ? DMA_SPACE_PCIE : DMA_SPACE_PCIE1,
    dma_req.des_mode = 0; //DMA_PCIE_RR2;

    dma_req.end_mode   = 0;
    dma_req.start_mode = (char) DMA_START_CHAN(MEAS_DMA_CHAN);
    //dma_req.intr_mode  = DMA_INTR_ENA;
    dma_req.wait_mode  = DMA_WAIT_INTR | DMA_WAIT_1S | (5<<4);

    //dma_req.size = size; //dma_buf->size | DMA_SIZE_PKT_128;
    dma_req.size = 0x400;

    status = tsc_dma_alloc(ifcdevice->node, MEAS_DMA_CHAN);
    if (status) 
    {
        LOG((LEVEL_ERROR, "%s() tsc_dma_alloc(MEAS_DMA_CHAN) == %d \n", __FUNCTION__, status));
        tsc_dma_clear(ifcdevice->node, MEAS_DMA_CHAN);
        tsc_dma_free(ifcdevice->node, MEAS_DMA_CHAN);
        goto dmaerr;
    }

    status = tsc_dma_move(ifcdevice->node, &dma_req);
    if (status) 
    {
        LOG((LEVEL_ERROR, "%s() tsc_dma_move() == %d status = 0x%08x\n", __FUNCTION__, status, dma_req.dma_status));
        tsc_dma_clear(ifcdevice->node, MEAS_DMA_CHAN);
        tsc_dma_free(ifcdevice->node, MEAS_DMA_CHAN);
        goto dmaerr;
    }

    /* Check for errors */
    if (dma_req.dma_status & DMA_STATUS_TMO)
    {
        LOG((LEVEL_ERROR, "DMA ERROR -> timeout | status = %08x\n",  dma_req.dma_status));
        tsc_dma_clear(ifcdevice->node, MEAS_DMA_CHAN);
        tsc_dma_free(ifcdevice->node, MEAS_DMA_CHAN);
        goto dmaerr;
    }
    else if(dma_req.dma_status & DMA_STATUS_ERR)
    {
        LOG((LEVEL_ERROR, "DMA ERROR -> unknown error | status = %08x\n",  dma_req.dma_status));
        tsc_dma_clear(ifcdevice->node, MEAS_DMA_CHAN);
        tsc_dma_free(ifcdevice->node, MEAS_DMA_CHAN);
        goto dmaerr;
    }
    
    /*free DMA channel 0 */
    status = tsc_dma_free(ifcdevice->node, MEAS_DMA_CHAN);
    if (status) 
    {
      LOG((LEVEL_ERROR, "%s() tsc_dma_free() == %d\n", __FUNCTION__, status));
      tsc_dma_clear(ifcdevice->node, MEAS_DMA_CHAN);
      goto dmaerr;
    }

    /* Copy from kernel space (dma_buf) to userspace (pp_options)
     * TODO: check for errors and fill buffer with zeros??
     */       
    memcpy(dest_buffer, dma_buf->u_base, (size_t) 0x400);
    tsc_kbuf_free(ifcdevice->node, dma_buf);
    free(dma_buf);
    return 0;

dmaerr:
    tsc_kbuf_free(ifcdevice->node, dma_buf);
    free(dma_buf);
    return status;
#else
    ifcdaqdrv_status status;
    int cmdword = RDWR_MODE_SET( 0x44, RDWR_SPACE_USR1, 0);
    void *mybuffer = calloc(0x400,1);

    if (!ifcdaqdrv_is_byte_order_ppc()) {
        cmdword = tsc_swap_32(cmdword);
    }

    ulong src_addr = (ulong) sram_addr;

    usleep(100);
    status = tsc_read_blk(ifcdevice->node, src_addr, (char*) mybuffer, 0x400, cmdword);

    if (status) {
        LOG((LEVEL_ERROR,"tsc_blk_read() returned %d\n", status));
        free(mybuffer);
        return status;
    }

    memcpy(dest_buffer, mybuffer, 0x400);

    // if (ifcdaqdrv_is_byte_order_ppc()) {
    //     ifcdaqdrv_swap64(rt_status);
    // }
    free(mybuffer);

    return status_success;   

#endif
}


#define RT_OFFSET 0x300000
ifcdaqdrv_status ifcfastintdrv_read_rtstatus(struct ifcdaqdrv_dev *ifcdevice, uint32_t addr, uint64_t *rt_status) 
{
    ifcdaqdrv_status status;
    int cmdword = RDWR_MODE_SET( 0x44, RDWR_SPACE_USR1, 0);
    void *mybuffer = calloc(1024*1024,1);

    if (!ifcdaqdrv_is_byte_order_ppc()) {
        cmdword = tsc_swap_32(cmdword);
    }

    ulong src_addr = RT_OFFSET + addr;

    usleep(1000);
    status = tsc_read_blk(ifcdevice->node, src_addr, (char*) mybuffer, sizeof(*rt_status), cmdword);

    if (status) {
        LOG((LEVEL_ERROR,"tsc_blk_read() returned %d\n", status));
        free(mybuffer);
        return status;
    }

    memcpy((void *)rt_status, mybuffer, sizeof(*rt_status));

    if (ifcdaqdrv_is_byte_order_ppc()) {
    	ifcdaqdrv_swap64(rt_status);
	}
    free(mybuffer);

    return status_success;
}


ifcdaqdrv_status ifcfastintdrv_fastreset_specialilck(struct ifcdaqdrv_dev *ifcdevice) {
    ifcdaqdrv_status status;
    void *mybuffer = calloc(1024*1024,1);
	
    /* Prepares the command word for CPU copy using tsclib (based on TscMon source code)*/
    int cmdword = RDWR_MODE_SET( 0x44, 4, 0);
    
    /* if running on little endian machine the command word needs to be converted */
    if (!ifcdaqdrv_is_byte_order_ppc()) {
        cmdword = tsc_swap_32(cmdword);
    }

    uint32_t fpga_mem_address1 = 0x700;
    uint32_t fpga_mem_address2 = 0x700 + 0x10; 

    /* Sets the PP_OPTION addres of the specific pp block */
    ulong dest_addr1 = IFCFASTINT_SRAM_PP_OFFSET + fpga_mem_address1;
    ulong dest_addr2 = IFCFASTINT_SRAM_PP_OFFSET + fpga_mem_address2;

    uint64_t pp_options1, pp_options2 = 0;

    pthread_mutex_lock(&ifcdevice->lock);

    status = ifcfastintdrv_read_pp_conf(ifcdevice, fpga_mem_address1, &pp_options1);
	if(status) {
		pthread_mutex_unlock(&ifcdevice->lock);
		return status_internal;
	}

    status = ifcfastintdrv_read_pp_conf(ifcdevice, fpga_mem_address2, &pp_options2);
	if(status) {
		pthread_mutex_unlock(&ifcdevice->lock);
		return status_internal;
	}

	uint64_t pp_options_unswapped1 = pp_options1;
	uint64_t pp_options_unswapped2 = pp_options2;

    // For some unknown reason (firmware?) it is not guaranteed that writes to 64bits register actually happen 
    // that is why the write functions are repeated for 3 times

	// ------------- set MODE to ONE to reset block -------------------------------------
	
	pp_options1 = u64_setclr(pp_options1, 1, 1, 61); //autoreset
	pp_options1 = u64_setclr(pp_options1, 1, 0xF, 56); //mode
	pp_options2 = u64_setclr(pp_options2, 1, 1, 61); //autoreset
	pp_options2 = u64_setclr(pp_options2, 1, 0xF, 56); //mode

    /* if running on BIG endian machine, the 8-byte long word needs to be converted to big endian */
    if (ifcdaqdrv_is_byte_order_ppc()) {
    	ifcdaqdrv_swap64(&pp_options1);
        ifcdaqdrv_swap64(&pp_options2);
    }
    
    memcpy(mybuffer, (void *)&pp_options1, sizeof(pp_options1));
    usleep(500);
    status = tsc_write_blk(ifcdevice->node, dest_addr1, (char*) mybuffer, sizeof(pp_options1), cmdword);
    status = tsc_write_blk(ifcdevice->node, dest_addr1, (char*) mybuffer, sizeof(pp_options1), cmdword);
    status = tsc_write_blk(ifcdevice->node, dest_addr1, (char*) mybuffer, sizeof(pp_options1), cmdword);
    
    memcpy(mybuffer, (void *)&pp_options2, sizeof(pp_options2));
    usleep(500);
    status = tsc_write_blk(ifcdevice->node, dest_addr2, (char*) mybuffer, sizeof(pp_options2), cmdword);
    status = tsc_write_blk(ifcdevice->node, dest_addr2, (char*) mybuffer, sizeof(pp_options2), cmdword);
    status = tsc_write_blk(ifcdevice->node, dest_addr2, (char*) mybuffer, sizeof(pp_options2), cmdword);

	// ------------- set MODE to 0b11 (3) to restart block -------------------------------------

	pp_options_unswapped1 = u64_setclr(pp_options_unswapped1, 1, 1, 61); //autoreset
	pp_options_unswapped1 = u64_setclr(pp_options_unswapped1, 3, 0xF, 56); //mode
	pp_options_unswapped2 = u64_setclr(pp_options_unswapped2, 1, 1, 61); //autoreset
	pp_options_unswapped2 = u64_setclr(pp_options_unswapped2, 3, 0xF, 56); //mode

    /* if running on BIG endian machine, the 8-byte long word needs to be converted to big endian */
    if (ifcdaqdrv_is_byte_order_ppc()) {
    	ifcdaqdrv_swap64(&pp_options_unswapped1);
        ifcdaqdrv_swap64(&pp_options_unswapped2);
    }

    memcpy(mybuffer, (void *)&pp_options_unswapped1, sizeof(pp_options_unswapped1));
    usleep(500);
    status = tsc_write_blk(ifcdevice->node, dest_addr1, (char*) mybuffer, sizeof(pp_options_unswapped1), cmdword);
    status = tsc_write_blk(ifcdevice->node, dest_addr1, (char*) mybuffer, sizeof(pp_options_unswapped1), cmdword);
    status = tsc_write_blk(ifcdevice->node, dest_addr1, (char*) mybuffer, sizeof(pp_options_unswapped1), cmdword);
    
    memcpy(mybuffer, (void *)&pp_options_unswapped2, sizeof(pp_options_unswapped2));
    usleep(500);
    status = tsc_write_blk(ifcdevice->node, dest_addr2, (char*) mybuffer, sizeof(pp_options_unswapped2), cmdword);
    status = tsc_write_blk(ifcdevice->node, dest_addr2, (char*) mybuffer, sizeof(pp_options_unswapped2), cmdword);
    status = tsc_write_blk(ifcdevice->node, dest_addr2, (char*) mybuffer, sizeof(pp_options_unswapped2), cmdword);

    free(mybuffer);
    if (status) {
        LOG((LEVEL_ERROR,"tsc_blk_write() returned %d\n", status));
        pthread_mutex_unlock(&ifcdevice->lock);
        return status;
    }

    pthread_mutex_unlock(&ifcdevice->lock);
    return status_success;
}
